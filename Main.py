# -*- coding: utf-8 -*-
#
# Darkwing4
#

def openFile():
    """читает из файла msg наше сообщение и трет символы не из алфавита
        препод не знал, что в текстовом редакторе есть кнопка "переносить строки" и походу делал это энтером \n
    """
    encryptMessage = ""
    for line in open('msg', 'r', encoding='utf-8'):
        encryptMessage += line[:len(line)-1].upper()
    for i in encryptMessage:
        if i not in alpha:
            encryptMessage = encryptMessage.replace(i, '')
    return encryptMessage


def lenKey(txt):
    """ реализация автокорреляционного метода
        возвращает длину ключа, иначе выводим на экран [index_list]
    """
    index_list = [] # массив индекса совпадений
    lenMsg = len(txt)

    # считаем что 2 - минимальный размер ключа, ибо шифровать 1-символьным ключем будет только совсем овощ
    shift_txt = txt[2:] + txt[:2]
    for shift in range(2, len(txt)//10):
        count = 0 # число совпавших символов(с одинаковым индексом) изначальной строки со сдвинутой на shift символов
        for i in range(lenMsg):
            if txt[i] == shift_txt[i]: count += 1
        index_list.append(round(count * 100 / lenMsg, 3)) # добавляем индекс сопадений в масив

        shift_txt = shift_txt[1:] + shift_txt[:1] # снова двигаем строку на 1 символ

    inx = [] # индексы больших "индексов совпадений" в [index_list]
    for i in range(1, len(index_list)):
        try:
            if index_list[i-1] < index_list[i] > index_list[i+1] and index_list[i] > 4.4: # определяем скачек
                inx.append(index_list.index(index_list[i]) + 2) # записываем на каких по счету сдвигах был скачек
        except IndexError: # обработка границы массиова
            if index_list[i-1] < index_list[i] and index_list[i] > 4.4:
                inx.append(index_list.index(index_list[i]) + 2) # прибавляем минмиальную длину ключа
    if inx[1] - inx[0] == inx[2] - inx[1]: # этого условия достаточно в большинстве случаев
        return inx[1] - inx[0] # вернет число кратное длине ключа
    else:
        print("обнаружена аномалия, требуется ручной анализ данных автокорреляционного метода")
        print("Вывод индексов на экран:\n")
        print(index_list)
        return input('Введите предпологаемую длину ключа: ')


def textFormat(txt, lenKey):
    """ Принимает исходный текст и длину ключа
        Разбивает текст на lenKey элементов
        Возвращает список из lenKey строк
    """
    l = [""] * lenKey
    for i in range(0, len(txt), lenKey):
        split_txt = txt[i:i + lenKey]
        try:
            for q in range(lenKey):
                l[q] += split_txt[q]
        except IndexError:
            None
    return l


def letterFrequency(line):
    """ Принимает строку
        Возвращает массив с числом(не путать с частотой) символов для каждого символа алфавита
        [8, 13, 7, 7, 0, 2, 0, 4, 1, 0, 0, 0, 2, 2, 0, 2, 29, 13, 4, 9, 9, 7, 16, 3, 2, 17, 1, 8, 10, 10, 14, 22, 4]
    """
    letterFrequencyList = [0] * len(alpha)
    for letterIndex, letter in enumerate(alpha):
        letterFrequencyList[letterIndex] = line.count(letter)
    return letterFrequencyList


def caesar(txt, _step):
    result = ''
    for c in txt:
        result += alpha[(alpha.index(c) + _step) % len(alpha)]
    return result


def matchIndex(letter_count1, letter_count2, num_line, step=0):
    """ На вход подается массивы частот символов 0-го и n-го столбца
        На выходе получаем сдвиг n-го столбца относительно 0-го
    """
    try:
        res = 0
        for i in range(len(alpha)):
            res += letter_count1[i] * letter_count2[i]
        if 0.053 <= round(res/(len(lines[0])*len(lines[num_line])), 3) <= 0.07:
            return step # вернет нужный сдвиг
        new_letter_count2 = letterFrequency(caesar(lines[num_line], step + 1))

        if step > len(alpha):
            raise RecursionError

        return matchIndex(letter_count1, new_letter_count2, num_line, step + 1)

    except RecursionError:
        print("Похоже какой-то ящер уронил лишний символ в зашифрованный текст. Ну либо алфавит неправильный.")
        print("Заданный алфавит:", alpha, "Зашифрованное сообщение:", text, sep="\n")
        print("Бог в помощь.")
        exit(-1488)


def findKey(lines):
    letter_counts = []
    for line in lines:
        letter_counts.append(letterFrequency(line))

    keys = [0] * (len(lines) - 1)
    for i in range(0, len(lines)-1):
        keys[i] = matchIndex(letter_counts[0], letter_counts[i + 1], i + 1)

    for indexLetter in range(len(alpha)):
        word = alpha[indexLetter]
        for key in keys:
            word += alpha[indexLetter - key]
        print(word)


def decryptVigener(encryptMessage, keyWord):
    keyWord *= len(encryptMessage) // len(keyWord) + 1
    message = ""
    for index, letter in enumerate(encryptMessage):
        message += alpha[(alpha.index(letter) - alpha.index(keyWord[index])) % len(alpha)]
    return message


if __name__ == "__main__":

    alpha = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ "

    text = openFile()  # читаем и чистим текст от левых символов

    lenKey = lenKey(text)   # вычисляем предпологаемую длину ключа
    print("Предпологаемая длина ключа: ", lenKey)

    lines = textFormat(text, lenKey)  # разбиваем text на lenKey частей и делаем из них массив lines

    findKey(lines) # находим сдвиги всех столбцов относительно первого

    print(decryptVigener(text, input("Введите предпологаемое кодовое слово: ").upper()))
    input()
